#!/bin/bash

cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system_ext/app/TrichromeWebView6432/TrichromeWebView6432.apk.* 2>/dev/null >> system_ext/app/TrichromeWebView6432/TrichromeWebView6432.apk
rm -f system_ext/app/TrichromeWebView6432/TrichromeWebView6432.apk.* 2>/dev/null
cat system_ext/app/TrichromeLibrary6432/TrichromeLibrary6432.apk.* 2>/dev/null >> system_ext/app/TrichromeLibrary6432/TrichromeLibrary6432.apk
rm -f system_ext/app/TrichromeLibrary6432/TrichromeLibrary6432.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
